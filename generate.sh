#!/bin/bash

# brew install protobuf
# brew install protoc-gen-go-grpc

export PATH=$PATH:$GOPATH/bin

protoc ./src/services/greet/greetpb/greet.proto --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative
protoc ./src/services/calculator/calculatorpb/calculator.proto --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative