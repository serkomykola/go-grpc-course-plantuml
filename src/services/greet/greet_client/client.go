package main

import (
	"context"
	"fmt"
	"go-grpc-course/src/services/greet/greetpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"io"
	"log"
	"time"
)

func main() {
	fmt.Println("Hello, I am a client")

	conn, err := grpc.Dial(
		"localhost:50051",
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		log.Fatalf("Client could not connect: %v", err)
	}

	defer conn.Close()

	c := greetpb.NewGreetServiceClient(conn)

	//doUnary(c)
	//doServerStreaming(c)
	//doClientStreaming(c)
	doBiDiStreaming(c)
}

func doUnary(c greetpb.GreetServiceClient) {
	fmt.Println("Starting to do Unary RPC...")
	res, err := c.Greet(context.Background(), &greetpb.GreetRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Nikolas",
			LastName:  "Serko",
		},
	})

	if err != nil {
		log.Fatalf("Error while calling Greet RPC: %v", err)
	}

	fmt.Printf("Response from Greet: %v \n", res)
}

func doServerStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Starting to do Server Streaming RPC...")

	resStream, err := c.GreetManyTimes(context.Background(), &greetpb.GreetManyTimesRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Nikolas",
			LastName:  "Serko",
		},
	})

	if err != nil {
		log.Fatalf("Error while calling Greet RPC: %v", err)
	}

	for {
		msg, err := resStream.Recv()
		if err == io.EOF {
			// we've reach the end of the stream
			break
		}

		if err != nil {
			log.Fatalf("Error while reading stream: %v", err)
		}

		fmt.Printf("Response from GreetManyTimes: %v \n", msg.GetResult())
	}
}

func doClientStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Starting to do Client Streaming RPC...")

	requests := []*greetpb.LongGreetRequest{
		&greetpb.LongGreetRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Serko",
			},
		},
		&greetpb.LongGreetRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Nikolas",
			},
		},
	}

	stream, err := c.LongGreet(context.Background())
	if err != nil {
		log.Fatalf("Error creating reading stream: %v", err)
	}

	// Send every request individually
	for _, req := range requests {
		fmt.Printf("Sending the request: %v \n", req)
		stream.Send(req)

		time.Sleep(100 * time.Millisecond)
	}

	result, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error while receiving log greet response: %v", err)
	}

	fmt.Printf("Response from LongGreet: %v \n", result)
}

func doBiDiStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Starting to do BiDi Streaming RPC...")

	// Create a stream by invoking the client
	stream, err := c.GreetEveryone(context.Background())
	if err != nil {
		log.Fatalf("Error while creating stream: %v", err)
	}

	requests := []*greetpb.GreetEveryoneRequest{
		&greetpb.GreetEveryoneRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Serko",
			},
		},
		&greetpb.GreetEveryoneRequest{
			Greeting: &greetpb.Greeting{
				FirstName: "Nikolas",
			},
		},
	}
	waitc := make(chan struct{})

	// Send a bunch of messages to the client
	go func() {
		for _, req := range requests {
			fmt.Printf("Sending req: %v\n", req)
			err = stream.Send(req)
			time.Sleep(1000 * time.Millisecond)
		}
		stream.CloseSend()
	}()

	// Receive a bunch of messages from the server
	go func() {
		for {
			msg, err := stream.Recv()
			if err == io.EOF {
				break
			}

			if err != nil {
				log.Fatalf("Error while reading stream: %v", err)
				break
			}

			fmt.Printf("Response from GreetEveryone: %v \n", msg.GetResult())
		}
		close(waitc)
	}()

	<-waitc
}
