package main

import (
	"context"
	"fmt"
	"go-grpc-course/src/services/greet/greetpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"net"
	"strconv"
	"time"
)

type server struct {
	greetpb.UnimplementedGreetServiceServer
	//greetpb.UnimplementedGreetServiceServer
}

func (*server) Greet(ctx context.Context, in *greetpb.GreetRequest) (*greetpb.GreetResponse, error) {
	fmt.Printf("Greet funciton was involked with %v \n", in)
	firstName := in.GetGreeting().GetFirstName()
	result := "Hello " + firstName

	res := &greetpb.GreetResponse{
		Result: result,
	}

	return res, nil
}

func (*server) GreetManyTimes(req *greetpb.GreetManyTimesRequest, stream greetpb.GreetService_GreetManyTimesServer) error {
	fmt.Printf("GreetManyTimes function was involked with %v \n", req)
	firstName := req.GetGreeting().GetFirstName()

	for i := 0; i < 5; i++ {
		result := "Hello " + firstName + " number " + strconv.Itoa(i)
		res := &greetpb.GreetMayTimesResponse{
			Result: result,
		}

		err := stream.Send(res)
		if err != nil {
			fmt.Printf("Error while send: %v \n", err)
		}

		time.Sleep(1000 * time.Millisecond)
	}

	return nil
}

func (*server) LongGreet(stream greetpb.GreetService_LongGreetServer) error {
	fmt.Printf("LongGreet function was involked with a streaming request  \n")
	result := "Hello "

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			// we've reach the end of the stream
			break
		}

		if err != nil {
			log.Fatalf("Error while reading client stream: %v \n", err)
		}

		firstName := req.GetGreeting().GetFirstName()
		result += firstName + ", "
	}

	return stream.SendAndClose(&greetpb.LongGreetResponse{
		Result: result,
	})
}

func (*server) GreetEveryone(stream greetpb.GreetService_GreetEveryoneServer) error {
	fmt.Printf("GreetEveryone function was involked with a streaming request  \n")

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			// we've reach the end of the stream
			return status.Error(codes.InvalidArgument, "request message not received")
		}

		if err != nil {
			log.Fatalf("Error while reading client stream: %v \n", err)
			return err
		}

		fmt.Printf("GreetEveryone request: %v\n", req)

		firstName := req.GetGreeting().GetFirstName()
		result := "Hello " + firstName
		err = stream.Send(&greetpb.GreetEveryoneResponse{
			Result: result,
		})

		if err != nil {
			log.Fatalf("Error while sending data to client: %v \n", err)
			return err
		}
	}
}

func main() {
	fmt.Printf("Hello Server!\n")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	s := grpc.NewServer()

	greetpb.RegisterGreetServiceServer(s, &server{})
	if err = s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
