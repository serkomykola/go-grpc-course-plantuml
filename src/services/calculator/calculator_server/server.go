package main

import (
	"context"
	"fmt"
	"go-grpc-course/src/services/calculator/calculatorpb"
	"google.golang.org/grpc"
	"log"
	"net"
)

type server struct {
	calculatorpb.UnimplementedCalculatorServiceServer
	//greetpb.UnimplementedGreetServiceServer
}

func (*server) Sum(ctx context.Context, in *calculatorpb.SumRequest) (*calculatorpb.SumResponse, error) {
	fmt.Printf("Sum funciton was involked with %v \n", in)
	res := &calculatorpb.SumResponse{
		SumResult: in.GetFirstNumber() + in.GetSecondNumber(),
	}

	return res, nil
}

func main() {
	fmt.Printf("Sum Server!\n")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	s := grpc.NewServer()

	calculatorpb.RegisterCalculatorServiceServer(s, &server{})
	if err = s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
