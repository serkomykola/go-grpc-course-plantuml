package main

import (
	"context"
	"fmt"
	"go-grpc-course/src/services/calculator/calculatorpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

func main() {
	fmt.Println("Hello, I am a client")

	conn, err := grpc.Dial(
		"localhost:50051",
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		log.Fatalf("Client could not connect: %v", err)
	}

	defer conn.Close()

	c := calculatorpb.NewCalculatorServiceClient(conn)
	doUnary(c)
}

func doUnary(c calculatorpb.CalculatorServiceClient) {
	fmt.Println("Starting to do Unary RPC...")
	res, err := c.Sum(context.Background(), &calculatorpb.SumRequest{
		FirstNumber:  2,
		SecondNumber: 3,
	})

	if err != nil {
		log.Fatalf("Error while calling Greet RPC: %v", err)
	}

	fmt.Printf("Response from Greet: %v \n", res)
}
